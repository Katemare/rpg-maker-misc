# Eng/Англ: Makes true and false respond to .is_a?(Boolean)
# Rus/Рус: Позволяет распознать true и false запросом .is_a?(Boolean)
module Boolean
end
class TrueClass
  include Boolean
end
class FalseClass
  include Boolean
end

# позволяет сконвертировать любой объект в буль в явном виде. когда условие
# проверяет истинность объекта, оно не конвертирует его, а отталкивается от своих
# представлений об истинных и ложных объектах (поэтому nil считается лодным,
# хотя не является false).
class Object
  
  def to_b
    return true if self
    return false
  end
  alias :to_boolean :to_b
  alias :to_bool    :to_b
  
  def to_lazy_bool
    return false if [false, nil, 0, 0.0, '0', 'false', 'no', 'off', 'n'].include?(self)
    return true
  end
  
end