# Eng/Англ: These classes store a set of Game_Character objects - 'Stack' - as
# a data structure. It is then used to display a held stack, on the head of
# a character, in hands of a character, on a table and so on.

# Rus/Рус: Эти классы представляют собой структуру данных для хранения стопки
# объектов Game_Character, которая используется для отображения на голове
# персонажа, в руках персонажа, на столе и так далее.