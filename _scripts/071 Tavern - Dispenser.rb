# WIP

# событие является источником предметов, которые можно взять.
class EventTemplate_Dispenser < EventTemplate
  
  attr_lazy_bool :preview_offer, on_change: :reveal_offer_maybe
  
  DEFAULT_DATA=
  {
    preview_offer: true # показывать ли "верхний" предмет (на своей клетке).
  }
  
  def activate
    reset_offer
  end
  
  def get_offer
    unless @offer
      @offer=create_offer
      place_offer if @preview_offer
    end
    return @offer
  end
  
  def place_offer
    var_dump 'placing for '+@event.id.to_s
    if @event.carrier?
      @event.carry(@offer)
    else
      @offer.moveto(@event.real_x, @event.real_y)
    end
  end
  
  def create_offer
    # override me!
  end
  
  def reveal_offer
    offer=get_offer
    return unless offer.erased?
    var_dump offer.introduce
    place_offer
  end

  def reveal_offer_maybe
    p 'maybe?'
    reveal_offer if @preview_offer
  end
  
  def reset_offer
    var_dump 'preview_offer='+@preview_offer.inspect+' for '+@event.id.to_s
    if @preview_offer
      get_offer
    else
      @offer=nil
    end
  end
  
  def dispense(character)
    return unless character.carrier?
    offer=get_offer
    return unless offer
    return unless character.stack.can_add?(offer)
    if @event.carrier? and @event.carries?(offer)
      return unless @event.expel(offer)
    end
    return unless character.carry(offer)
    reset_offer
  end
	
end

# событие является источником однородных предметов, которые можно взять.
class EventTemplate_MonoDispenser < EventTemplate_Dispenser
  
  attr_int :source_map_id, on_change: :reset_prototype
  attr_int :source_event_id, on_change: :reset_prototype
  
  MONODISPENSER_DEFAULT_DATA=
	{
    source_map_id: nil,
    source_event_id: nil
	}
  
  def default_data
    super+MONODISPENSER_DEFAULT_DATA
  end
  
  def create_offer
    $game_map.create_event(get_prototype)
  end
  
  def get_prototype
    unless @prototype
      @prototype=DataManager.get_event(@source_map_id, @source_event_id)
    end
    return @prototype
  end
  
  def reset_prototype
    @prototype=nil
    reset_offer
  end
end

class Game_Event
  attr_reader :event
end