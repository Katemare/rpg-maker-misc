# адаптировано отсюда: http://www.rpgmakervxace.net/topic/786-copy-events/

module DataManager
  def self.get_event(map_id, event_id)
    map_id=$game_map.map_id if map_id.nil? and $game_map
    return unless map_id
    
    map=load_data(sprintf("Data/Map%03d.rvdata2", map_id))
    return map.events[event_id]
  end
end

class Game_Event
  include Disposable
  # стандартно событие удаляется только уборщиком, когда уходят все ссылки
  # на него (с карты и спрайта). команда "временно стереть" только делает
  # событие невидимым. добавляемый функционал позволяет вернуть временно
  # стёртое событие или удалить событие насовсем.
  
  attr_accessor :id # необходимо для установки айди новосозданным событиям.
  
  alias :spawnEvent_moveto :moveto
  def moveto(x, y)
    manifest
    spawnEvent_moveto(x, y)
  end
  
  # позволяет получить данные о том, является ли событие временно стёртым.
  def erased?
    @erased
  end
  
  # возвращает временно стёртое событие.
  def manifest
    return unless erased?
    @erased=false
    refresh
  end

  # вызывается методом dispose, когда требуется удалить событие.
  def execute_dispose
    $game_map.remove_event(self)
  end
  
end

# функционал, обслуживающий удаление и добавление персонажей.
class Spriteset_Map
  
  # персонаж представлен на карте?
  def includes_character?(character)
    @character_sprites.any? { |sprite| sprite.character==character }
  end
  
  # добавлен новый персонаж.
  def add_character(character)
    return if includes_character?(character)
    @character_sprites.push(Sprite_Character.new(@viewport1, character))
  end
  
  # персонаж удалён. предполагает, что у персонажа только один спрайт.
  def remove_character(character)
    sprite=@character_sprites.find { |sprite| sprite.character==character }
    return unless sprite
    
    sprite.dispose
    @character_sprites.delete(sprite)
  end
  
end

class Game_Map
  
  # удалить событие (принимает объект Game_Event)
  def remove_event(event)
    return unless @events.include?(event)
    @events.delete(event)
    @tile_events.delete(event)
    SceneManager.scene.spriteset.remove_character(event)
  end
  
  # клонировать события с указанной карты. можно поставить на заданные координаты
  def clone_event(map_id, event_id, x=nil, y=nil)
    proto=DataManager.get_event(map_id ? map_id : @map_id, event_id)
    return unless proto
    create_event(proto, true, x, y)
  end
  
  # создать событие по заданным данным. можно указать айди, координаты.
  def create_event(data, id=true, x=nil, y=nil)
    if id.nil?
      id=data.id
    elsif id==true
      id=get_free_event_id
    end
    event=Game_Event.new(@map_id, data)
    event.id=id
    add_event(event, x, y)
    return event
  end
  
  # добавить уже созданное событие (Game_Event), можно указать координаты.
  def add_event(event, x=nil, y=nil)
    return event.equal?(@events[event.id]) if @events.has_key?(event.id)
    @events[event.id]=event
    @tile_events[event.id]=event if @tile_events and event.tile?
    SceneManager.scene.spriteset.add_character(event) \
     if SceneManager.scene.is_a?(Scene_Map) 
    if x.nil? or y.nil?
      event.erase
    else
      event.moveto(x,y)
    end
    return true
  end
  
  # получить неиспользуемый айди события.
  def get_free_event_id
    @events.keys.max+1
  end
end

class Game_Player
  def do_test_action
    $xx=0 unless $xx
    $game_map.clone_event(nil, 7, $xx, 0)
    $xx+=1
  end
end