# Eng/Англ: The 'abstract' base class assumes either Array or Hash for a storage
# var. The stack calls the following methods of a 'thing', if they are available:
#  fits_stack?(stack, slot)
#  added_to_stack(stack, slot)
#  expelled_from_stack(stack, slot)

#
# Rus/Рус: Этот "абстрактный" класс предполагает использование Array или Hash для
# хранения. Стопка также обращается к следующим методам вещи, если таковые имеются:
#  fits_stack?(stack, slot) - годится ли для добавления в стопку?
#  added_to_stack(stack, slot) - когда вещь добавлена в стопку
#  expelled_from_stack(stack, slot) - когда вещь удалена из стопки

# dependancy/зависимость: PixelShift, EventSelfSwitches

class CarryStack
  
  CARRIER_PLAYER=true
  CARRIER_EVENT=false
  
  def self.make_carrier?(character)
    return CARRIER_PLAYER if character.is_a?(Game_Player)
    return CARRIER_EVENT if character.is_a?(Game_Event)
    return false
  end
  
  attr_reader :stack # for debug / для отладки.
  
  # to override / для переопределения наследниками:
  #  create_stack
  #  full?
  #  valid_slot?(slot)
  #  free_slot?(slot)
  #  find_slot_for(thing)
  #  insert(thing, slot)
  #  remove(thing)
  
  def initialize
    @stack=create_stack
  end
  
  def [](slot)
    @stack[slot]
  end
  
  def empty? # пусто?
    @stack.empty?
  end
  
  def fits?(thing, slot=nil) # подходит?
    # nil slot allowed for performing a generic query
    # аргумент nil допустим \ ради прокерки общего характера
    return false if has?(thing)
    return false if full?
    if slot!=nil
      return false unless valid_slot?(slot)
      return false unless free_slot?(slot)
    end
    return false unless good_content?(thing, slot)
    return false if thing.respond_to?(:fits_stack?) \
     and !thing.fits_stack?(self, slot)
    return true
  end
  
  def good_content?(thing, slot=nil) # подходящее_содержимое?
    thing.is_a?(Game_Event)
  end
 
  def can_add?(thing, slot=nil)
    return false if full?
    slot=find_slot_for(thing) if slot.nil?
    return false if slot.nil?
    return false unless fits?(thing, slot)
    return true
  end
  
  def add(thing, slot=nil) # добавить
    slot=find_slot_for(thing) if slot.nil?
    return false unless can_add?(thing, slot)
    return false unless insert(thing, slot)
    added_to_stack(thing, slot)
    thing.added_to_stack(self, slot) \
     if thing.respond_to?(:added_to_stack)
    return true
  end
  
  def added_to_stack(thing, slot)
  end
  
  def expel(thing) # изъять
    return false unless has?(thing)
    former_slot=find_slot_of(thing)
    return false unless remove(thing)
    expelled_from_stack(thing, former_slot)
    thing.expelled_from_stack(self, former_slot) \
      if thing.respond_to?(:expelled_from_stack)
    return true
  end
  
  def expelled_from_stack(thing, former_slot)
  end
  
  def expel_at(slot) # изъять_из_ячейки
    return false unless valid_slot?(slot)
    return false if free_slot?(slot)
    expel @stack[slot]
  end
  
  def has?(thing) # внутри?
    @stack.include?(thing)
  end
 
  def any?(&block) # есть?
    @stack.any?(&block)
  end
 
  def all?(&block) # все?
    @stack.all?(&block)
  end
 
  def count(suffices=nil) # подсчёт
    return size unless block_given?
    count=0
    @stack.each { |thing|
      count=count+1 if yield(thing)
      return if suffices!=nil and count>=suffices
    }
    return count
  end
  
  def enough?(needed_count, &block) # хватает?
    return false if needed_count>size
    return count(needed_count, &block) >= needed_count
  end
 
  def size # размер
    @stack.size
  end
  
  def full? # набито?
    # override me
  end
  
  def valid_slot?(slot) # действительная_ячейка?
    return true
    # override me
  end
  
  def free_slot?(slot) # ячейка_свободна?
    return true
    # override me
  end
  
  def find_slot_of(thing) # найти_ячейку (где вещь)
    @stack.index(thing)
  end
  
  private
  
  def create_stack # создать_стопку
    # override me
  end
  
  def find_slot_for(thing) # подобрать_ячейку (для вещи)
    # override me
  end
  
  def insert(thing, slot) # вставить
    # override me
  end
  
  def remove(thing) # удалить
    # override_me
  end
      
end

class Spriteset_Map
  alias :carryStack_create_characters :create_characters
  
  def create_characters
    carryStack_create_characters
    create_carry_stacks
  end
  
  def create_carry_stacks
    @carry_stacks=[]
    @character_sprites.each do |sprite|
      character=sprite.character
      character.push_stacks(@carry_stacks) if character.carrier?
    end
  end
  
  def update_characters
    refresh_characters if @map_id != $game_map.map_id
    stacks_updated=false
    @character_sprites.each do |sprite|
      if not stacks_updated and sprite.character.carried? then
        update_carry_stacks 
        stacks_updated=true
      end
      sprite.update
    end
    update_carry_stacks unless stacks_updated
  end
  
  def update_carry_stacks
    @carry_stacks.each do |stack|
      stack.update
    end
  end
  
  def sort_character_sprites!(&block)
    return unless block_given?
    @character_sprites.sort!(&block)
  end
end

class Scene_Map
  attr_reader :spriteset
end

class Game_CharacterBase
  
  alias :carryStack_initialize :initialize
  alias :carryStack_shift_y :shift_y
  
  attr_accessor :carried_in
  
  def initialize
    carryStack_initialize
    init_carrier if CarryStack.make_carrier?(self)
  end
  
  def init_carrier
    # override me
  end
  
  def carrier?
    # override me
  end
  
  def carried?
    return true if @carried_in
    return false
  end
  
  def carried_depth
    return 0 unless carried?
    return @carried_in.carrier.carried_depth+1
  end
  
  def shift_y
    return 0 if carried?
    carryStack_shift_y
  end
  
  def push_stacks(stacks)
  end
  
  def to_top(layer=0)
    @priority_type=2+layer
  end
  
  def to_middle
    @priority_type=1
  end
  
  def to_bottom
    @priority_type=0
  end
  
end

class Game_Event
  alias :carryStack_check_event_trigger_touch :check_event_trigger_touch
  alias :carryStack_trigger_in? :trigger_in?
  alias :carryStack_setup_page_settings :setup_page_settings
  
  def check_event_trigger_touch(x, y)
    return false if carried?
    carryStack_check_event_trigger_touch(x,y)
  end
  
  def trigger_in?(triggers)
    return false if carried? and [0,1,2].include?(@trigger)
    carryStack_trigger_in?(triggers)
  end
  
  def setup_page_settings
    carryStack_setup_page_settings
    carried_in.adjust_carried(self) if carried?
  end
end