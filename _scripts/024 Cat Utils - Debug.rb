# Eng/Англ: A basic utility script for debug purposes.
# Rus/Рус: Простой скрипт с функциями, полезными для отладки.

# привычное по php и некоторым другим языкам название функции.
# по такому названию удобно искать "хвосты" вывода отладки
# метод p() сам делает содержимому inspect и возвращает содержимое же!
alias :var_dump :p

# возможность из любого места вызвать halt, чтобы на 10 секунд остановить
# всякое выполнение (например, чтобы посмотреть изменения во фрейме).
module Kernel
  def halt(secs=10)
    p 'Halting!'
    sleep secs
    p 'Resuming!'
  end
end

# учит объекты представляться комбинацией класса и уникального айди.
class Object
  def introduce
    self.class.name+self.object_id.to_s
  end
end

# приказать представиться всем объектам в массиве или хэше.
module Enumerable
  def introduce_elements
    self.each { |value| p value.introduce }
  end
end

# добавляет точку для создания тестового действия по F5.
# dependancy/зависимость: Cat Controls - Custom
class Game_Player
  alias :varDump_check_custom_input :check_custom_input
  
  def check_custom_input
    return true if varDump_check_custom_input
    return do_test_action if Input.trigger?(:F5)
    return false
  end
  
  # точка обработки тестового действия. возвращает либо ложь, если обработки
  # не было, либо истину, если была.
  def do_test_action
    return false
  end
end