# Eng/Англ: The 'abstract' class that moves a Sprite.
# Rus/Рус: Это "абстрактный" класс, непосредственно двигающий спрайт.
class SpritePathManager
  include Setupable
  include Startable
  include Disposable
  
  DEFAULT_DURATION=60 # 1 second / 1 секунда
  
  attr_reader   :subject  # субъект
  attr_reader   :tracer   # траектория
  attr_reader   :timing   # прогресс
  
  def initialize(subject)
    @subject=subject
  end
  
  def setup_args
    [:subject, :tracer, :timing]
  end
  
  def timing=(value)
    @timing.dispose if @timing
    @timing=value
    @timing.bind(:start) { self.start_path }
    @timing.bind(:tick) { self.tick }
    @timing.bind(:finish) { self.finish_path }
  end
  
  def tracer=(value)
    @tracer.dispose if @tracer
    @tracer=value
    @tracer.start
  end
  
  def update  # обновить
    return unless started?
    @timing.update
  end
  
  def tick    # тик
    @tracer.percent=@timing.percent
  end
  
  def x
    # override me
  end
  
  def y
    # override me
  end
  
  def start_path  # начало траектории
  end
  
  def finish_path # конец траектории
    dispose
  end
  
  def execute_dispose
    @timing.dispose
    @timing=nil
    @tracer.dispose
    @tracer=nil
  end
end

class CharPathManager < SpritePathManager
  
  attr_accessor :preserve_pos # сохранять позицию
  attr_accessor :shift_x # temp
  attr_accessor :shift_y # temp
  
  def initialize(*args)
    @shift_x=0
    @shift_y=0
    @preserve_pos=false
    super
    @subject.charpath=self
  end
  
  def start_path
    preserve_pos
    super
  end
  
  def preserve_pos   # сохранить позицию
    @start_x=@subject.x
    @start_y=@subject.y
  end
  
  def finish_path
    # PixelShift dep
    if @preserve_pos
      @subject.draw_at(@start_x+@shift_x, @start_y+@shift_y)
    else
      @subject.draw_at(@subject.real_x+@shift_x, @subject.real_y+@shift_y)
    end
    super
  end
  
  def x
    @tracer.x
  end
  
  def y
    @tracer.y
  end
  
  def real_x
    x
  end
  
  def real_y
    x
  end
  
  def screen_x
    $game_map.adjust_x(x) * 32 + 16
  end
  
  def screen_y
    $game_map.adjust_y(y) * 32 + 32 - @subject.shift_y
  end
  
  def execute_dispose
    super
    @subject.charpath=nil if @subject.charpath===self
  end
  
end

class Game_CharacterBase
  
  attr_reader :charpath
  
  def charpath?
    return true if @charpath
    false
  end
  
  def charpath=(val)
    return if val===@charpath
    @charpath.dispose if charpath?
    @charpath=val
  end
  
  alias :spritePath_update :update
  def update
    spritePath_update
    @charpath.update if charpath?
  end
  
  alias :spritePath_screen_x :screen_x
  def screen_x
    return @charpath.screen_x if charpath?
    spritePath_screen_x
  end
  
  alias :spritePath_screen_y :screen_y
  def screen_y
    return @charpath.screen_y if charpath?
    spritePath_screen_y
  end
  
  alias :charPath_pivot_x :pivot_x
  def pivot_x
    return @charpath.real_x if charpath?
    charPath_pivot_x
  end
  
  alias :charPath_pivot_y :pivot_y
  def pivot_y
    return @charpath.real_y if charpath?
    charPath_pivot_y
  end
end
