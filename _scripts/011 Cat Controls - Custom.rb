# Этот скрипт добавляет точки для вставки собственного управления помимо
# стандартного. К сожалению, базовый движок не предусматривает таких точек,
# так что пришлось скопипастить код и добавить недостающие строчки.
# В случае использования со скриптом Free Movement тот должен быть
# включён после данного!

class Game_Player
  def update_nonmoving(last_moving)
    return if $game_map.interpreter.running?
    if last_moving
      $game_party.on_player_walk
      return if check_touch_event
    end
    if movable?
      if Input.trigger?(:C)
        return if get_on_off_vehicle
        return if check_action_event
        return if check_default_action  # добавлено
      else                              # добавлено
        return if check_custom_input    # добавлено
      end
    end
    update_encounter if last_moving
  end

  # точка для вставки стандартного действия, выполняющегося по кнопке действия
  # в случае отсутствия взаимодействия с событием или слезания с транспорта.
  # должно возвращать ложь, если обработки не было, и истину в случае обработки.
  def check_default_action
    false
  end
  
  # точка для вставки произвольного ввода. 
  # должно возвращать ложь, если обработки не было, и истину в случае обработки.
  def check_custom_input
    false
  end

end