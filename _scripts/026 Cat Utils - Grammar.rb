# это методы, делающие код более красивым в некоторых случаях.

class Object
  alias :is_an? :is_a?
  # для применения перед Integer и т.д.
  # можно использовать kind_of? , но если уж использовать is_a? , то
  # должно быть и is_an?
  
  def callable?
    respond_to? :call
  end
end

module Callable
  def self.===(val)
    super or val.callable?
  end
end

class Proc
  include Callable
end
class Method
  include Callable
end
class UnboundMethod
  include Callable
end

# для случаев, когда название переменной в единственном числе.
class Array
  alias :includes? :include?
end

class Hash
  alias :includes? :include?
  
  def +(val)
    if val.is_a?(Hash)
      self.merge(val)
    else
      raise 'Bad Hash+'
    end
  end
end