# WIP
# Добавляет дополнительную координату позиционирования спрайтов персонажей - Z,
# олицетворяющей высоту над поверхностью (не путать с порядком приоритета
# спрайтов). Это нужно для того, чтобы вещи, отображающиеся высоко над головой
# персонажа не считались находящимися на несколько клеток дальше по оси Y и
# не пытались взаимодействовать с окружающими их там предметами. Ось Z рабоатает
# так же, как оси X и Y.

# К сожалению, это не полноценная реализация высоты, поскольку какую бы высоту
# в воображаемом мире ни олицетворяли тайлы, игрой они будут восприниматься
# как 0 по оси Z. Персонаж, поднятый по координате Z, не сможет перелетать
# барьеры или правильно отбрасывать теперь на разновысотный ландшафт.

class Game_CharacterBase
  attr_reader :z
  attr_reader :real_z
  
  alias :realZ_init_public_members :init_public_members
  def init_public_members
    realZ_init_public_members
    @z=0.0
    @real_z=0.0
  end
  
  # мгновенное перемещение на заданную высоту
  def moveto_z(val)
    @z=val
    @real_z=val
    update_bush_depth
  end
  
  # постепенное перемещение на заданную высоту
  def float_to(val)
    @z=val
  end
  
  # добавить к высоте заданную дистанцию (постепенно)
  def z_rise(val)
    @z+=val
  end
  
  # отнять от высоты заданную дистанцию (постепенно)
  def z_drop(val)
    @z=[@real_z-val, 0.0].max
  end
  
  # приравнять высоту к 0 (постепенно)
  def z_land
    @z=0.0
  end
  
  alias :realZ_moving? :moving?
  def moving?
    realZ_moving? or @real_z != @z
  end
  
  alias :realZ_update_move :update_move
  def update_move
    @real_z = [@real_z - vertical_distance_per_frame, @z].max if @z < @real_z
    @real_z = [@real_z + vertical_distance_per_frame, @z].min if @z > @real_z
    realZ_update_move
  end
  
  # вертикальная скорость перемещения (по умолчанию равга горизонтальной)
  def vertical_distance_per_frame
    distance_per_frame
  end
  
  alias :realZ_update_bush_depth :update_bush_depth
  def update_bush_depth
   realZ_update_bush_depth
   @bush_depth=[0, @bush_depth-@real_z].max if @bush_depth>0 and @real_z!=0
  end
 
  alias :realZ_screen_y :screen_y
  def screen_y
    realZ_screen_y-(@real_z*32)*z_compression
  end

  # коэффициент сжатия вертикальных дистанций из-за угла обзора.
  def z_compression
    0.7
  end
  
end