# Это "абстрактный" класс для стопок, имеющих определённое число именованных
# символами ячеек, например, для показа экипировки на персонаже или еды на подносе.

class CarryStack_Slotted < CarryStack
  
  def initialize(slots=nil)
    super()
    setup_slots(slots)
  end  
  
  def create_stack
    Hash.new
  end
  
  def setup_slots(new_slots) # задать_ячейки
    if @slots!=nil
      raise 'Deleting filled slot' \
       if @stack.keys.any? { |key| !new_slots.has_key?(key) }
    end
    
    if new_slots==nil then
      @slots=[]
    else
      @slots=new_slots.uniq
    end
  end

  def add_slot(slot) # добавить_ячейку
    return if @slots.include?(slot)
    @slots.push(slot)
  end
  
  def add_slots(slots) # добавить_ячейки
    slots.each { |slot| add_slot(slots) }
  end
  
  def delete_slot(slot) # удалить_ячейку
    raise 'Deleting filled slot' if @stack.has_key?(slot)
    @slots.delete(slot)
  end
  
  def delete_slots(slots) # удалить_ячейки
    slots.each { |slot| delete_slot(slots) }
  end
  
  def full?
    @stack.size==@slots.size
  end
  
  def valid_slot?(slot)
    @slots.include?(slot)
  end
  
  def free_slot?(slot)
    !@stack.has_key?(slot) 
  end
  
  def find_slot_for(thing)
    return @slots.find { |slot| fits?(thing, slot) }
  end
  
  def insert(thing, slot)
    @stack[slot]=thing
  end
  
  def remove(thing)
    @stack.delete(slot(thing))
  end
  
end