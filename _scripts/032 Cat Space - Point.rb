# Eng/Англ: This 'abstract' class supplies map coords.
# Rus/Рус: Этот "абстрактный" класс поставляет координаты на карте.
class Point
  def x
    # override me
  end
  
  def y
    # override me
  end
end

# Eng/Англ: This class supplies constant coords.
# Rus/Рус: Этот класс поставляет координаты фиксированной точки.
class Point_Fixed < Point
  
  attr_reader :x
  attr_reader :y
  
  def initialize(x, y)
    @x=x
    @y=y
  end
  
  def self.from_char(char) # фиксирует текущие реальные координаты персонажа.
    new(char.real_x, char.real_y)
  end
  
end

# Eng/Англ: This class supplies character coords.
# Rus/Рус: Этот класс поставляет координаты персонажа.
class Point_Character < Point
  attr_accessor :char     # персонаж
  attr_accessor :shift_x  # смещение по горизонтали
  attr_accessor :shift_y  # смещение по вертикали
  
  def initialize(char, shift_x=0, shift_y=0)
    @char=char
    @shift_x=shift_x
    @shift_y=shift_y
  end
  
  def x
    @char.real_x+@shift_x
  end
  
  def y
    @char.real_y+@shift_y
  end
end