# позволяет удобно получать заданный класс по названию
# или допиской к названию заданного класса.

class Class
  
  def self.class_from_string(str)
    Object.const_get(str)
  end
end

# подключается через extend и расширяет возможности определения класа.
module Spawns_subclasses
  def class_from_keyword(keyword)
    Class.class_from_string(self.name+'_'+keyword)
  end
end

# подключается через extend и расширяет возможности определения класа.
module Attr_processor
  
  attr_reader :on_change
  attr_reader :attr_processors
  
  # вызывается в контексте класса
  def attr_processor(attr_name, opts={}, &body)
    
    define_processor_methods
    attr_reader(attr_name)
    register_attr_processor(attr_name, &body)
    if on_change=opts[:on_change]
      register_on_change(attr_name, on_change)
    end
    
    define_method attr_name.to_s+'=' do |val|
      # вызывается в контексте экземпляра
      set_processed_attr(attr_name, val, &body)
      return instance_var_get(attr_name)
    end
    
  end
  
  def define_processor_methods
    unless method_defined?(:set_processed_attr)
      define_method :set_processed_attr do |attr_name, val, run_on_change=true|
        # вызывается в контексте экземпляра
        var_name='@'+attr_name.to_s
        var_content=instance_variable_get(var_name)
        return false if var_content.eql?(val)
        processed=process_attr(attr_name, val)
        var_dump self.class.attr_processors
        var_dump '!!!'+attr_name.to_s+' is '+processed.inspect
        return false if var_content.eql?(processed)
        instance_variable_set(var_name, processed)
        on_attr_change(attr_name) if run_on_change
        return true
      end
    end
    
    unless method_defined?(:mass_set_processed_attr)
      define_method :mass_set_processed_attr do |attrs, run_on_change=true|
        # вызывается в контексте экземпляра
        return if attrs.empty?
        changed=[]
        attrs.each_pair do |attr_name, val|
          changed << attr_name if set_processed_attr(attr_name, val)
        end
        var_dump changed
        if run_on_change
          changed.each do |attr_name|
            on_attr_change(attr_name)
          end
        end
      end
    end
    
    unless method_defined?(:on_attr_change)
      define_method :on_attr_change do |attr_name|
        # вызывается в контексте экземпляра
        return unless self.class.on_change.has_key?(attr_name)
        case on_change=self.class.on_change[attr_name]
        when nil
          # nop
        when Symbol
          send(on_change)
        when Callable # Cat Utils Grammar dep
          on_change.call
        end   
      end
    end
    
    unless method_defined?(:process_attr)
      define_method :process_attr do |attr_name, val|
        # вызывается в контексте класса
        return val unless self.class.attr_processors and self.class.attr_processors.has_key?(attr_name)
        return self.class.attr_processors[attr_name].call(val)
      end
    end
  end
  
  # вызывается в контексте класса
  def register_on_change(attr_name, on_change)
    @on_change={} unless @on_change
    @on_change[attr_name]=on_change
  end
  
  # вызывается в контексте класса
  def register_attr_processor(attr_name, &processor)
    @attr_processors={} unless @attr_processors
    @attr_processors[attr_name]=processor
  end
  
  # все вызываются в контексте класса
  def attr_bool(attr_name, opts={})
    attr_processor(attr_name, opts) { |val| val.to_b } # Cat Utils Boolean dep
  end
  
  def attr_lazy_bool(attr_name, opts={})
    attr_processor(attr_name, opts) { |val| val.to_lazy_bool } # Cat Utils Boolean dep
  end
  
  def attr_int(attr_name, opts={})
    attr_processor(attr_name, opts) { |val| val.to_i }
  end
  
  def attr_str(attr_name, opts={})
    attr_processor(attr_name, opts) { |val| val.to_s }
  end
  
  def attr_sym(attr_name, opts={})
    attr_processor(attr_name, opts) { |val| val.to_sym }
  end
    
end

class Object
  # реализует "late static binding" для констант объекта. в противном случае
  # при вызове родительского метода (super) этот метод будет иметь доступ
  # к константам класса, где он был объявлен, даже если они были переопределены
  # верхним классом self.
  def late_const_get(sym, inherit=true)
    self.class.const_get(sym, inherit)
  end
end

class String
  def to_class
    Class.class_from_string(self)
  end
end