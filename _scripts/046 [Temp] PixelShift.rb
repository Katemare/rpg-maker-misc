# WIP?
# Это простой скрипт пиксельного смещения спрайта персонажа.

module PixelShift
  CELL_WIDTH=32
  
  def self.to_pixels(grid_span)
    grid_span*CELL_WIDTH
  end
  
  def self.to_grid(pixel_span)
    pixel_span/CELL_WIDTH
  end
end

class Game_CharacterBase
  attr_accessor :ox
  attr_reader :oy # has a special setter method / значение задаётся особым методом.
  
  alias :pixelShift_initialize  :initialize
  alias :pixelShift_update_bush_depth :update_bush_depth
  alias :pixelShift_screen_x    :screen_x
  alias :pixelShift_screen_y    :screen_y
  
  def initialize
    pixelShift_initialize
    reset_pixel_shift
  end
  
  def reset_pixel_shift
    @ox=0
    @oy=0
  end
  
  def set_pixel_shift(ox, oy)
    @ox=ox
    @oy=oy
  end
  
  def add_pixel_shift(ox, oy)
    @ox+=ox
    @oy+=oy
  end
  
  def oy=(val)
    @oy=val
    update_bush_depth
  end
  
  def update_bush_depth
   pixelShift_update_bush_depth
   @bush_depth=[0, @bush_depth-@oy].max if @bush_depth>0 and @oy!=0
  end
 
  def screen_x
    pixelShift_screen_x+@ox
  end

  def screen_y
    pixelShift_screen_y+@oy
  end
  
  # Eng/Англ: no straighten or other disrupting calls, just changes the position of the sprite.
  # Rus/Рус: не вызывает ни staighten, ни других методов, меняющих отображение
  # спрайта - только задаёт его расположение.
  def draw_at(x, y, ox=0, oy=0)
    @x=x
    @y=y
    @real_x=x
    @real_y=y
    @ox=ox
    @oy=oy
  end
  
end