# Тестирование стопки-башни.

class Game_Player
  
  attr_reader :stack
  
  def init_carrier
    @stack=CarryStack_Tower.new(self)
  end
  
  def carrier?
    @stack.to_bool
  end
  
  def carries?(thing)
    @stack.has?(thing)
  end
  
  def push_stacks(stacks)
    stacks.push(@stack)
  end
  
  def carry(thing)
    unless @stack.add(thing)
      Sound.play_buzzer
      var_dump ':('
    else
      true
    end
  end
  
  def expel(thing)
    @stack.expel(thing)
  end
  
end