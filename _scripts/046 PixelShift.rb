# WIP?
# Это простой скрипт пиксельного смещения спрайта персонажа.

module PixelShift
  CELL_WIDTH=32
  
  def self.to_pixels(grid_span)
    grid_span*CELL_WIDTH
  end
  
  def self.to_grid(pixel_span)
    pixel_span/CELL_WIDTH
  end
end

class Game_CharacterBase
  attr_accessor :ox
  attr_reader   :oy # has a special setter method / значение задаётся особым методом.
  attr_accessor :oz # отвечает только за порядок отображения! не имеет отношения к высоте.
  
  alias :pixelShift_init_public_members  :init_public_members
  def init_public_members
    pixelShift_init_public_members
    init_pixel_shift
  end
  
  def init_pixel_shift
    @ox=0
    @oy=0
    @oz=0
  end
  
  def reset_pixel_shift
    self.ox=0
    self.oy=0
    self.oz=0
  end
  
  def set_pixel_shift(ox, oy, oz=nil)
    self.ox=ox
    self.oy=oy
    self.oz=oz unless oz.nil?
  end
  
  def add_pixel_shift(ox, oy, oz=nil)
    self.ox+=ox
    self.oy+=oy
    self.oz+=oz unless oz.nil?
  end
  
  def oy=(val)
    @oy=val
    update_bush_depth
  end
  
  alias :pixelShift_update_bush_depth :update_bush_depth
  def update_bush_depth
   pixelShift_update_bush_depth
   @bush_depth=[0, @bush_depth-@oy].max if @bush_depth>0 and @oy!=0
  end
 
  alias :pixelShift_screen_x    :screen_x
  def screen_x
    pixelShift_screen_x+@ox
  end

  alias :pixelShift_screen_y    :screen_y
  def screen_y
    pixelShift_screen_y+@oy
  end
  
  alias :pixelShift_screen_z    :screen_z
  def screen_z
    pixelShift_screen_z+@oz
  end
  
  # Eng/Англ: no straighten or other disrupting calls, just changes the position of the sprite.
  # Rus/Рус: не вызывает ни staighten, ни других методов, меняющих отображение
  # спрайта - только задаёт его расположение.
  def draw_at(x, y, ox=0, oy=0, oz=nil)
    @x=x
    @y=y
    @real_x=x
    @real_y=y
    set_pixel_shift(ox, oy, oz)
  end
  
end

class Game_Event
  
  alias :pixelShift_setup_by_page_comments :setup_by_page_comments
  def setup_by_page_comments
    pixelShift_setup_by_page_comments
    
    if set_ox=get_param_int(:shift_x, false)
      self.ox=set_ox
    end
    if set_oy=get_param_int(:shift_y, false)
      self.oy=set_oy
    end
    if set_oz=get_param_int(:shift_z, false)
      self.oz=set_oz 
    end
  end

end