# Устанавливает тестовое действие (по F5) на прочерчивание игроком круга.
# dependancy/зависимости: Cat Controls Custom, Cat Debug

class Game_Player
  def do_test_action
    return if $game_player.charpath
    
    charpath=CharPathManager.new($game_player)
    
    charpath.tracer=Trace_Arc.new( \
      start_point: Point_Fixed.new(real_x, real_y), \
      end_point: Point_Fixed.new(real_x+3, real_y), \
      height: 3 \
      )
      
    charpath.timing=Timing.new(30)
      
    charpath.shift_x=3
    
    charpath.start
  end
end