# WIP
#| <template>
#| 
#| 
#| 
#| 
#| 

# экземпляры этого класса подцепляются к событиям (Game_Event), чтобы обеспечить
# меняющийся функционал и отслеживания данных, прежде всего согласно специально
# сформированным комментариям.
class EventTemplate
  extend Spawns_subclasses  # Cat Utils - Class dep
  extend Attr_processor     # Cat Utils - Class dep
	include Setupable         # Cat Utils - For Inclusion dep
	
	DEFAULT_DATA={}   # данные по умолчанию. они же являются списком полей.
	
	attr_reader :event # событие, к которому подцепился шаблон.
	
	def initialize(event, data)
		@event=event
		setup(default_data+data)
    activate
	end
	
	def default_data
		late_const_get(:DEFAULT_DATA)
	end
	
  # аргументом установщика являются те символы, которые засветились
  # в данных по умолчанию.
	def setup_arg?(sym)
		default_data.has_key?(sym)
	end
  
  def setup_args
    default_data.keys
  end
  
  # вызывается после того, как шаблон применён к событию и настроен.
  def activate
    #override me!
  end

end

class Game_Event

	alias :eventTemplate_init_private_members :init_private_members
	def init_private_members
		eventTemplate_init_private_members
		@templates=[] # список шаблонов, упорядоченный по времени добавления (первые раньше)
	end

  # добавляет новый шаблон согласно коду (классу) и данным.
	def apply_template(template_code, template_data)
		template=EventTemplate.class_from_keyword(template_code).new(self, template_data)
		@templates.push(template)
	end
	
  # пытается найти искомый метод в шаблонах.
	def respond_to?(method_name)
		return true if super
		@templates.each { |template| return true if template.respond_to?(method_name) }
		return false
	end
	
  # пытается найти искомый метод в шаблонах.
	def method_missing(method_name, *args)
    if @templates
      @templates.each { |template| return template.send(method_name, *args) if template.respond_to?(method_name) }
    end
		super
	end
  
  # ищет в комментариях параметры, задающие шаблоны, и создаёт их.
  alias :eventTemplate_setup_by_page_comments :setup_by_page_comments
  def setup_by_page_comments
    eventTemplate_setup_by_page_comments
    
    template_params=get_param_array(:template, nil)
    return unless template_params and not template_params.empty?
    setup_templates_by_comments(template_params)
  end
  
  TEMPLATE_TITLE_EX=/^\s*template_code\s*=\s*([a-z_0-9]+)\s*$/i
  TEMPLATE_DATA_EX=/^\s*([a-z_0-9]+)\s*=\s*(.+)\s*$/i
  
  # вызывается только если найдены шаблоны, заданные комментариями.
  def setup_templates_by_comments(template_params)
    template_code=nil
    template_data={}
    
    template_params.each do |comment|
      if comment=~TEMPLATE_TITLE_EX then
        apply_template(template_code, template_data) if template_code
        template_code=$~[1]
        template_data={}
      elsif comment=~TEMPLATE_DATA_EX then
        template_data[$~[1].to_sym]=$~[2]
      end
    end
    apply_template(template_code, template_data) if template_code
  end
  
end