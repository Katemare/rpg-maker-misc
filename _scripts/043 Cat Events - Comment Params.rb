# Eng/Англ: Extracts custom parameters set in specially formatted comments
# in beginning of an event page. The format follows:
#
# #keyword content
# ...or...
# <keyword>
# content
# content
# </keyword>
#
# Parameters can then be used by other scripts.

# Rus/Рус: Этот скрипт извлекает специальные параметры, записанные в особенно
# отформатированных комментариях в начале страницы события. Формат следующий:
#
# #ключевое_слово содержимое
# ещё содержимое
# ...или...
# <ключевое_слово>
# содержимое
# ещё содержимое
# </ключевое_слово>

class Game_Event
  
  PARAM_EX    =/^\#([a-z_0-9]+)(\s+(.+))?/i
  GROUP_EX    =/^<([a-z_0-9]+)>\s*$/i
  GROUP_END_EX=/^<\/([a-z_0-9]+)>\s*$/i
  
  def get_special_params(page_num=nil)
    if page_num.nil?
      page=@page
      page_num=@event.pages.index(page)
    elsif page_num.is_an?(Integer)
      page=@event.pages[page_num]
    elsif page_num.is_a?(RPG::Event::Page)
      page=page_num
      page_num=@event.pages.index(page)
    end
    return unless page
    if page_num \
     and @special_params_cache \
     and @special_params_cache.has_key?(page_num)
      return @special_params_cache[page_num]
    end
    
    commands=page.list
    group=nil
    key=nil
    index=0
    params={}
    while command=commands[index] do
      
      break unless command.code==108 or command.code==408
      # comment (108) or comment continuation (408)
      
      comment=command.parameters[0]
      if group and GROUP_END_EX=~comment and $~[1].to_sym==group 
        group=nil
        key=nil
      elsif group
        params[group].push(comment)
      elsif GROUP_EX=~comment
        group=$~[1].to_sym
        params[group]=[] unless params.has_key?(group)
      elsif PARAM_EX=~comment
        key=$~[1].to_sym
        params[key]=[] unless params.has_key?(key)
        params[key].push($~[3]) if $~[3]
      elsif key and command.code==408
        params[key].push(comment)
      else
        break
      end
      index+=1
    end
    @special_params_cache={} if  @special_params_cache.nil?
    @special_params_cache[page_num]=params if page_num
    return params
  end
  
  def get_single_param(code)
    params=get_special_params
    return unless params and params[code]
    return true if params[code].empty?
    # если ключевое слово есть, но не заполнено значением, то считается вкл.
    return params[code][-1] # последний элемент массива
  end
  
  def get_param_bool(code, default=false)
    param=get_single_param(code)
    return default if param.nil?
    return false if ['false', 'no', 'n', 'off', '0'].include?(param)
    return true if ['true', 'yes', 'y', 'on', '1'].include?(param)
    return default
  end
  
  def get_param_int(code, default=0)
    param=get_single_param(code)
    return default if param.nil?
    return param.to_i
  end
  
  def get_param_float(code, default=0.0)
    param=get_single_param(code)
    return default if param.nil?
    return param.to_f
  end
  
  def get_param_str(code, default='')
    param=get_single_param(code)
    return default if param.nil?
    return param.to_str
  end
  
  def get_param_length(code, default=0)
    param=get_single_param(code)
    return default if param.nil?
    return $~[1].to_f/PixelShift::CELL_WIDTH if /^\s*(\d+)px\s*$/i=~param
    return param.to_f
  end
  
  def get_param_pixels(code, default=0)
    param=get_single_param(code)
    return default if param.nil?
    return $~[1] if /^\s*(\d+)px\s*$/i=~param
    return PixelShift.to_pixels(param)
  end
  
  def get_param_array(code, default=[])
    param=get_special_params[code]
    return default if param.nil?
    return param
  end
  
  alias :commentParams_setup_page_settings :setup_page_settings
  def setup_page_settings
    commentParams_setup_page_settings
    setup_by_page_comments
  end
  
  def setup_by_page_comments
    # override me!
  end
end